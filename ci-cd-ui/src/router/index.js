import Vue from 'vue';
import Router from 'vue-router';

// Containers
import Full from '@/containers/Full';

// Views
import Dashboard from '@/views/Dashboard';
import Deployment from '@/views/Deployment';
import History from '@/views/History';
import Docker from '@/views/Docker';
import Settings from '@/views/Settings';

Vue.use(Router);

export default new Router({
  mode: 'hash',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard,
        },
        {
          path: 'deployment',
          name: 'Deployment',
          component: Deployment,
        },
        {
          path: 'history',
          name: 'History',
          component: History,
        },
        {
          path: 'docker',
          name: 'Docker',
          component: Docker,
        },
        {
          path: 'settings',
          name: 'Settings',
          component: Settings,
        },
      ],
    },
  ],
});

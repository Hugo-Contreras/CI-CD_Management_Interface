// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueResource from 'vue-resource';
import VueLogger from 'vuejs-logger';
import BootstrapVue from 'bootstrap-vue';
import VueMoment from 'vue-moment';
import Toasted from 'vue-toasted';
import VueLocalStorage from 'vue-localstorage';
import App from './App';
import router from './router';

const options = {
  // required ['debug', 'info', 'warn', 'error', 'fatal']
  logLevel: 'debug',
  // optional : defaults to false if not specified
  stringifyArguments: false,
  // optional : defaults to false if not specified
  showLogLevel: true,
  // optional : defaults to false if not specified
  showMethodName: false,
  // optional : defaults to '|' if not specified
  separator: '|',
  // optional : defaults to false if not specified
  showConsoleColors: false,
};

Vue.use(VueResource);
Vue.use(VueLogger, options);
Vue.use(BootstrapVue);
Vue.use(VueMoment);
Vue.use(Toasted, { duration: 4000 });
Vue.use(VueLocalStorage);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App,
  },
});

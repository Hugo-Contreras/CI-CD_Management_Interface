'use strict';

const merge = require('webpack-merge');
const prodEnv = require('./prod.env');

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // API_URL: "'http://10.132.241.106:3000'"
  API_URL: "'http://localhost:3000'",
  HOST: "'http://localhost'"
});

const express = require('express');
const mongoose = require('mongoose');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const bluebird = require('bluebird');
const cors = require('cors');
const dotenv = require('dotenv').config();

const config = require('./config');
const routes = require('./routes');
const settings = require('./service/settings');

const app = express();

mongoose.Promise = bluebird;
mongoose.connect(config.mongo.url, {
  useMongoClient: true
});

app.use(cors());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('tiny'));

app.use('/', routes);

// Init application settings
settings.init().then((settings) => {
  console.log(settings, 'Settings for the app.');
});

app.listen(config.server.port, () => {
  console.log(`Magic happens on port ${config.server.port}`);
});

module.exports = app;

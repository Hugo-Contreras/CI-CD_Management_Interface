const Controller = require('../../lib/controller');
const settingFacade = require('./facade');

class SettingController extends Controller {
  getConfig(req, res, next) {
    return this.facade.findLastOne({ createdAt: -1 })
      .then(doc => res.status(200).json(doc))
      .catch(err => res.status(500).json(err.message));
  }
}

module.exports = new SettingController(settingFacade);

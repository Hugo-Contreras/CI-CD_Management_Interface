const deployFacade = require('./facade');
const slackController = require('../../service/slack/controller');

class DeployController {
  constructor(facade) {
    this.facade = facade;
    this.slackController = slackController;
  }

  run(req, res, next) {
    return this.facade.run(req.body)
      .then(record => res.status(200).json(record))
      .catch(err => res.status(500).json(err.message))
      .then((result) => {
        this.slackController.sendDeploymentResult(result);
      });
  }

  find(req, res, next) {
    return this.facade.find(req.query)
      .then(collection => res.status(200).json(collection))
      .catch(err => res.status(500).json(err.message));
  }
}

module.exports = new DeployController(deployFacade, slackController);

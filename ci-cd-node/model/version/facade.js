const mongoose = require('mongoose');
const moment = require('moment');

const Facade = require('../../lib/facade');
const versionSchema = require('./schema');
const artifactoryAPI = require('../../service/artifactory');
const settings = require('../../service/settings');

class VersionFacade extends Facade {
  async getVersions(type) {
    const repository = await settings.getRepository();
    if (type === 'front') {
      return artifactoryAPI.getVersions(repository, type);
    } else if (type === 'back') {
      return artifactoryAPI.getVersions(repository, type);
    }
    throw Error('The type doesn\'t match either front or back');
  }

  checkIfCacheHasExpired(cachedValue) {
    const expirationDate = moment(cachedValue.requestedAt).add(10, 'minutes');
    return moment().isAfter(expirationDate);
  }

  cacheValue(value, oldValue, type) {
    let id = mongoose.Types.ObjectId();
    if (oldValue && oldValue._id) {
      id = oldValue._id;
    }
    this.model.update(
      { _id: id },
      {
        type,
        requestedAt: new Date(),
        versions: value.versions
      },
      { upsert: true }
    ).exec();
  }
}

module.exports = new VersionFacade('Version', versionSchema);

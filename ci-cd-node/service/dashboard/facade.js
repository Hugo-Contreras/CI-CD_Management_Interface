const deployFacade = require('../../model/deploy/facade');

class DashboardFacade {
  constructor(deployFacade) {
    this.deployFacade = deployFacade;
  }

  generateDashboardCards() {
    const environments = ['testing-mock', 'testing', 'staging-mock', 'staging', 'stable-mock', 'stable'];
    const dashboardCards = [];
    environments.forEach(async(environment) => {
      dashboardCards.push(this.deployFacade.findLastOne({ deploymentDate: -1 }, { environment, status: 'DEPLOYED' }, { environment: 1, frontVersion: 1, backVersion: 1 }));
    });
    return Promise.all(dashboardCards);
  }
}

module.exports = new DashboardFacade(deployFacade);

const dashboardFacade = require('./facade');

class DashboardController {
  constructor(facade) {
    this.facade = facade;
  }

  generateDashboardCards(req, res, next) {
    return this.facade.generateDashboardCards()
      .then((collectionArray) => {
        const dashboardCards = [];
        collectionArray.forEach((item) => {
          if (item) {
            dashboardCards.push(item);
          }
        });
        return res.status(200).json(dashboardCards);
      })
      .catch(err => res.status(500).json(err.message));
  }
}

module.exports = new DashboardController(dashboardFacade);

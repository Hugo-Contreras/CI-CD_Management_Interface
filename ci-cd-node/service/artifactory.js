const aql = require('jfrog-aql');

const settings = require('./settings');

class ArtifactoryAPI {
  async _init() {
    const server = await settings.getServer();
    const key = await settings.getKey();

    aql.config({
      uri: `${server}/api/search/aql`,
      headers: { 'X-JFrog-Art-Api': key }
    });
  }

  _requestArtifactory(repository, type) {
    if (repository && type) {
      const find = {
        type : 'file',
        repo: { $match : `${repository}*` }
      };
      if (type === 'front') {
        find.path = { $match : '*ui*' };
      } else if (type === 'back') {
        find.repo = { $match : `${repository}-m2*` };
      }
      const aqlQuery = aql.items.find(find).include('name', 'repo', 'path').sort({ $asc: ['name'] });

      return aql.query(aqlQuery);
    }
    throw Error('No repository url or type provided');
  }

  _getVersionNumber(artifact) {
    if (artifact) {
      const regex = /\d+.\d+.\d+(-SNAPSHOT)?/g;
      return artifact.match(regex)[0];
    }
    throw Error('No artifact object provided');
  }

  _extractVersions(artifacts) {
    if (artifacts.length) {
      return [...new Set(artifacts.map(element => this._getVersionNumber(element.name)))];
    }
    throw Error('No artifacts array provided');
  }

  async getVersions(repository, type) {
    // For demo purpose only
    return {
      versions: ['0.1.0-SNAPSHOT', '0.1.0', '0.2.0-SNAPSHOT', '0.2.0', '0.3.0-SNAPSHOT', '0.3.0', '0.4.0-SNAPSHOT', '0.4.0']
    };

    await this._init();
    const artifacts = await this._requestArtifactory(repository, type);
    return {
      versions: this._extractVersions(artifacts.results)
    };
  }
}

module.exports = new ArtifactoryAPI();

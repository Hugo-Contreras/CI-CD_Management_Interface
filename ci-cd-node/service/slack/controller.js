const { WebClient } = require('@slack/client');

class SlackController {
  constructor(key) {
    this.slackBot = new WebClient(key);
  }

  sayHelloToThomas(req, res, next) {
    this.slackBot.chat.postMessage('pioupiou', ':canada_goose: PIOU PIOU @tbrugier', { link_names: true })
      .then(response => res.status(200).json(response))
      .catch(err => res.status(500).json(err.message));
  }

  warnForDeployment(req, res, next) {
    if (req.body) {
      const environment = req.body.environment;
      let message;
      if (req.body.username) {
        message = `Warning: There will be a deployment on the integration server for ${environment.toUpperCase()}...if it breaks everything then blame the person responsible ! (BTW this person is ${req.body.username})`;
      } else {
        message = `Warning: There will be a deployment on the integration server for ${environment.toUpperCase()}...if it breaks everything then blame the person responsible !`;
      }
      this.slackBot.chat.postMessage('onedoc', message)
        .then(response => res.status(200).json(response))
        .catch(err => res.status(500).json(err.message));
    } else {
      return res.status(500).json('No environment provided !');
    }
  }

  sendDeploymentResult(deploymentResult) {
    const statusCode = deploymentResult.statusCode;
    const environment = deploymentResult.req.body.environment;
    let message;
    if (statusCode === 200) {
      message = `Deployment successful in ${environment.toUpperCase()} !`;
    } else {
      message = `Something went wrong during the deployment in ${environment.toUpperCase()} !`;
    }
    this.slackBot.chat.postMessage('onedoc', message)
      .catch(err => console.log(err));
  }
}

module.exports = new SlackController(process.env.SLACK_API_KEY);

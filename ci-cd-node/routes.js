const Router = require('express').Router;
const router = new Router();

// Entities
const user = require('./model/user/router');
const setting = require('./model/setting/router');
const deploy = require('./model/deploy/router');
const version = require('./model/version/router');

// Services
const dashboard = require('./service/dashboard/router');
const docker = require('./service/docker/router');
const slack = require('./service/slack/router');

/** @TODO
 * Delete this route if not used
 */
router.route('/').get((req, res) => {
  res.json({ message: 'Welcome to ci-cd-ui API !' });
});

// Entities
router.use('/user', user);
router.use('/setting', setting);
router.use('/deploy', deploy);
router.use('/version', version);

// Services
router.use('/dashboard', dashboard);
router.use('/docker', docker);
router.use('/slack', slack);

module.exports = router;
